package demo

fun main(args: Array<String>) {

    //RANGES
    println("\n\nRANGES")
    val oneTo10 = 1..10 //creates an array of numbers from 1 to 10
    val alpha = "A".."Z"    //creates an array of alphabets from A to Z
    println("R in Alpha : ${"R" in alpha}") //checks for R in the array of alphabets that was created before
    val tenTo1 = 10.downTo(1)   //create an array in descending order i.e., from 10 to 1
    val twoTo20 = 2.rangeTo(20)
    val range3 = oneTo10.step(3)    //steps by 3
    for(x in range3)
        println("range3 : $x")
    for(x in tenTo1.reversed()) //reversed() reverses the array elements
        println("Reverse : $x")

}