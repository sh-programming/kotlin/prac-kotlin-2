package demo
fun main(args: Array<String>){
    //null safety is built in kotlin
    /*
        we cannot assign a null value
        var nullValue: String = null
    */

    var nullValue: String? = null   //we can assign a null value by providing a ? before the datatype

    fun returnNull(): String?   //We give a ? after datatype if the function can return a null value
    {
        return null
    }

    val nullVal2 = returnNull()
    if (nullVal2 != null)
    {
        println("nullVal2.length")
    }

    //we can use force operator to force null assignments
    var nullVal3 = nullVal2!!.length    // !! is the force operator
    var nullVal4: String = returnNull() ?: "No Name"    // ?: Elvis operator is used to provide a default value
}