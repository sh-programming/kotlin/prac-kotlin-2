package demo
import java.util.Random //We can import java libraries in kotlin. Random library is used to find random values
fun main(args : Array<String>){
    //FUNCTIONS
    println("\n\nFUNCTIONS")
    fun add(num1: Int, num2: Int): Int = num1 + num2    //single line functions doesnt require return type
    println("5 + 4 = ${add(5,4)}")
    fun subtract(num1: Int = 1, num2: Int = 1): Int = num1 - num2   //we can give default values to parameters like this
    println("5 - 4 = ${subtract(5,4)}")
    println("4 - 5 = ${subtract(num2 = 5, num1 = 4)}")  //we can also give actual parameters by reference to the names of formal parameters
    fun sayHello(name: String): Unit = println("Hello $name")   //single line function without return type
    sayHello("Paul")

    val (two, three) = nextTwo(1)   //the pair of values returned by the nextTwo function is stored into two and three respectively
    println("1 $two $three")

    println("\nCALLING getSum()... Sum = ${getSum(1,2,3,4,5)}") //calling function

    val multiply = {num1: Int, num2: Int -> num1 * num2}    //function literals
    println("5 * 3 =  ${multiply(5, 3)}")   //calling function literal

    println("5! = ${fact((5))}")    //calling recursive function

    //HIGHER ORDER FUNCTIONS : Functions that accepts or returns another function
    val numList = 1..20
    val evenList = numList.filter { it % 2 == 0 }   //'it' can be used if a function only has one parameter
    println("\nPrinting even numbers:")
    evenList.forEach{ n -> println(n) }


    val mul3 = makeMath(3)
    println("5 * 3 = ${mul3(5)}")   //5 is value for num2 of the function that is returned by makeMath()

    val multiply2 = {num1: Int -> num1 * 2}
    val numList2 = arrayOf(1,2,3,4,5)
    mathonList(numList2, multiply2)
}



//FUNCTION THAT RETURNS TWO VALUES
fun nextTwo(num: Int): Pair<Int, Int>{
    return Pair(num+1, num+2)
}

//FUNCTION THAT ACCEPTS VARIABLE NUMBER OF ARGUEMENTS
fun getSum(vararg nums: Int): Int{
    var sum = 0
    nums.forEach{
        n -> sum += n
    }
    return sum
}

//RECURSIVE FUNCTIONS
fun fact(x: Int):Int{
    tailrec fun factTail(y: Int, z: Int): Int{
        if (y == 0) return z
        else return factTail(y - 1, y * z)
    }
    return factTail(x, 1)
}

//receives an integer num1 returns a function that multiplies num1 by num2 times
fun makeMath(num1: Int): (Int) -> Int = {num2 -> num1 * num2}

//accpets an integer array and a function an Int num is passed to the function which is a parameter
fun mathonList(numList: Array<Int>, myFunc: (num: Int) -> Int){
    for(num in numList){
        println("mathonList ${myFunc(num)}") //myFunc is the function that was accepted as a parameter
    }
}