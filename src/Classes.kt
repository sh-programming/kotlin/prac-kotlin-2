package demo

import com.sun.imageio.plugins.bmp.BMPImageReaderSpi

fun main(args : Array<String>){
    val bowser = Animal("Bowser", 20.0, 13.5)
    bowser.getInfo()

    val spot = Dog("Spot", 20.0, 14.5, "Smith")
    spot.getInfo()

    val tweety = Bird("Tweety", true)
    tweety.fly(10.0)
}

//classes are marked as final in java similarly here we mark it as open
open class Animal(val name: String,
                  val height: Double,
                  var weight: Double){
    //objects are initialized in init function
    init {
        //regular expression to match a number in any part of the string and make that string invalid if a number is present
        val regex = Regex(".*\\d+.*")
        require(!name.matches(regex)){ "Animal name can't contain numbers" }
        require(height > 0){ "Height should be greater than 0" }
        require(weight > 0){ "Weight should be greater than 0" }
    }

    //defining a class method
    open fun getInfo(): Unit    //return type Unit implies that it doesn't return anything
    {
        println("$name is $height tall and weighs $weight kilos")
    }
}

//here there is no open keyword so we cannot inherit from Dog class
class Dog(name: String,
          height: Double,
          weight: Double,
          var owner: String): Animal(name, height, weight)  //inherits Animal class
{
    override fun getInfo():Unit{
        println("$name is $height tall and weighs $weight kilos and is owned by $owner")
    }
}

//interface
interface Flyable{
    var flies: Boolean
    fun fly(distMile: Double): Unit
}
class Bird constructor(val name: String,
                       override var flies: Boolean = true)
    : Flyable{  //implementation of the interface
    override fun fly(distMile: Double): Unit {  //once the interface is implemented its methods should be overridden
        if(flies){
            println("$name flies $distMile miles")
        }
    }
}