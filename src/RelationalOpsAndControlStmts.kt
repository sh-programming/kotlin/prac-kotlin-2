package demo

import java.util.*

fun main(args: Array<String>) {

    //RELATIONAL AND LOGICAL OPERATORS
    println("\n\nLOGICAL AND RELATIONAL OPERATORS")
    val age = 8
    if(age < 5)
    {
        println("GO TO PRESCHOOL")
    }
    else if( age == 5){
        println("Go to kindergarten")
    }
    else{
        println("Go anywhere")
    }

    when(age){
        0,1,2,3,4 -> println("Go to preschool")
        5 -> println("Go to kindergarten")
        in 6..17 -> {
            val grade = age - 5
            println("Go to grade $grade")
        }
        else -> println("go to college")
    }

    //LOOPS
    println("\n\nLOOPS")
    for(x in 1..10){
        println("Loop : $x")
    }

    val rand = Random()
    val magicNum = rand.nextInt(50) + 1
    var guess = 0
    while(magicNum != guess){
        guess += 1
    }
    println("Magic number was : $guess")
    for(x in 1..20){
        if(x % 2 == 0){
            continue    //continue command
        }
        println("Odd : $x")
        if(x == 15)
            break   //break statement
    }

    var arr3: Array<Int> = arrayOf(3,6,9)
    for(i in arr3.indices){
        println("Mult 3 : ${arr3[i]}")
    }
    for((index,value) in arr3.withIndex()){
        println("Index : $index Value : $value")
    }


}