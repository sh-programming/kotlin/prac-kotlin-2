package demo
fun main(args : Array<String>){

    val map = mutableMapOf<Int, Any?>()

    val map2 = mutableMapOf(1 to "Doug", 2 to 25)

    map[1] = "Prabha"
    map[2] = 21
    println("Map size : ${map.size}")
    map.put(3, "Vaduthala") //adding a key value pair in a map
    map.remove(2)   //removing a key value pair by passing in a key

    //loop to iterate through all the available key value pairs in the map
    for((x, y) in map){
        println("Key : $x Value : $y")
    }
}